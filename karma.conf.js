/*jslint node: true */
'use strict';

module.exports = function(config) {
  config.set({
    basePath: './',
    frameworks: ['jasmine'],
    browsers: ['PhantomJS'],
    colors: true,
    files: [
      // lib files
      './node_modules/jquery/dist/jquery.js',
      './node_modules/angular/angular.js',
      './node_modules/angular-mocks/angular-mocks.js',
      './node_modules/jasmine-jquery/lib/jasmine-jquery.js',
      'http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDQBuPxdeRc2zw1w1Lf33f7KDd_p14v0es',

      // project files
      './src/client/app/app.module.js',
      './src/client/app/**/*.js',

      // fixtures
      {pattern: 'src/client/**/*.spec.json', watched: true, served: true, included: false}
    ],
    reporters: ['spec', 'coverage'],
    coverageReporter: {
      type: 'html',
      dir: 'coverage',
      includeAllSources: true
    },
    preprocessors: {
      './src/client/app/**/!(*spec).js': ['coverage']
    },
    singleRun: true
  });
};
