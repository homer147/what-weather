/*jslint node: true */
'use strict';

module.exports = function() {
  var client = './src/client/';
  var clientApp = client + 'app/';

  var config = {

    // File paths
    client: client,
    css: client + 'css/',
    fonts: [],
    html: client + '**/*.html',
    index: client + 'index.html',
    js: [
      clientApp + '**/*.module.js',
      clientApp + '**/*.js',
      '!' + clientApp + '**/*.spec.js'
    ],
    less: client + 'css/styles/**/styles.less',
    tests: clientApp + '**/*.spec.js',

    // NPM location
    packages: [
      './package.json'
    ]
  };

  return config;
};
