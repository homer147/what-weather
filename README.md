# What Weather Version 1.0

## Installation

Requires node.js and was built using v6.10.2.

Also recommended to have bower, gulp and karma installed globally. `npm install bower gulp karma -g`

Open cmd and navigate to the root of the repo, then download the required packages.

```npm install```

## Usage

After you have downloaded the packages, you can now build the css

```gulp build```

and then you can start the app

```npm start```

Now using your browser, navigate to http://localhost:3000.

Unit tests can be run by typing

```gulp test```

After the tests have run for the first time, you can find a 'Coverage' folder in the root directory. This contains a HTML file that reports on unit test coverage.

## Credits

Michael Browning

michael.f.browning@gmail.com
