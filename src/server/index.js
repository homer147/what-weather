/* jslint node: true */
'use strict';

const express = require('express');
const app = express();

app.use(express.static('./'));
app.use(express.static('./src/client/'));

app.listen(3000, function () {
  console.log('listening on port 3000');
});