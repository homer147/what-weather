/* jslint node: true */
/* global inject, expect, jasmine, getJSONFixture */

(function () {
  'use strict';

  describe('whatWeather main Controller', function () {
    var weatherServiceMock,
      createController,
      whatWeatherCtrl,
      scope,
      mockResponse,
      success,
      jsonData;

    beforeEach(function () {
      module('whatWeather');
    });

    beforeEach(function () {
      module(function ($provide) {
        $provide.service('weatherService', weatherServiceMock);
      });
    });

    beforeEach(function () {
      weatherServiceMock = {
        getWeather: jasmine.createSpy().and.callFake(function () {
          success = arguments[1];
          success(mockResponse);
        })
      };
    });

    beforeEach(inject(function ($rootScope, $controller) {
      jasmine.getJSONFixtures().fixturesPath = 'base/src/client/app';
      jsonData = getJSONFixture('app.module.spec.json');
      mockResponse = jsonData;

      createController = function () {
        scope = $rootScope.$new();

        return $controller('whatWeatherCtrl', {
          $scope: scope,
          weatherService: weatherServiceMock
        });
      };

      whatWeatherCtrl = createController();
    }));

    it('should set default variables', function () {
      expect(scope.model).toEqual([]);
      expect(scope.loading).toBeFalsy();
    });

    it('should get the weather for a place', function () {
      scope.getWeather('inala, Brisbane');

      expect(weatherServiceMock.getWeather).toHaveBeenCalled();
      expect(scope.model.error).toBeFalsy();
      expect(scope.loading).toBeFalsy();
      expect(scope.description).toBeDefined();
      expect(scope.today).toEqual(jasmine.any(Object));
      expect(scope.atmosphere).toBeDefined();
      expect(scope.forecast).toEqual(jasmine.any(Array));
    });

  });
}());
