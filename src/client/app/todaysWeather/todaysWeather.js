(function () {
  'use strict';

  var app = angular.module('whatWeather');

  app.directive('todaysWeather', function () {
    return {
      restrict: 'E',
      templateUrl: 'app/todaysWeather/todaysWeather.html',
    };
  });
}());
