(function () {
  'use strict';

  var app = angular.module('whatWeather', []);

  app.controller('whatWeatherCtrl', ['$scope', 'weatherService', function ($scope, weatherService) {
    $scope.model = [];
    $scope.loading = false;

    function weatherSuccess(res) {
      var tenDays = res.data.query.results.channel.item.forecast;

      $scope.model.error = false;
      $scope.loading = false;
      $scope.description = res.data.query.results.channel.item.title;
      $scope.today = tenDays[0];
      $scope.atmosphere = res.data.query.results.channel.atmosphere;
      $scope.forecast = tenDays.slice(1, 6);
    }

    function weatherFail(res) {
      $scope.loading = false;
      $scope.today = undefined;
      $scope.description = undefined;
      $scope.atmosphere = undefined;
      $scope.forecast = undefined;

      if (res.error) {
        console.log('error: ' + res.error);
        $scope.$apply(function () {
          $scope.model.error = true;
        });
      }
    }

    $scope.getWeather = function (place) {
      $scope.loading = true;
      weatherService.getWeather(place, function (res) {
        weatherSuccess(res);
      },

      function (res) {
        weatherFail(res);
      });
    };
  }]);

}());
