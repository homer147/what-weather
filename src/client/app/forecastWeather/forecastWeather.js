(function () {
  'use strict';

  var app = angular.module('whatWeather');

  app.directive('forecastWeather', function () {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/forecastWeather/forecastWeather.html',
    };
  });
}());
