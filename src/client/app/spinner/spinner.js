(function () {
  'use strict';

  var app = angular.module('whatWeather');

  app.directive('spinner', function () {
    return {
      restrict: 'E',
      templateUrl: 'app/spinner/spinner.html',
    };
  });
}());
