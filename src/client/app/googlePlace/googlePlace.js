/* global google */

(function () {
  'use strict';

  var app = angular.module('whatWeather');

  app.directive('googleplace', function () {
    return {
      require: 'ngModel',
      link: function (scope, element, attrs, model) {
        var options,
          pacItems,
          createViewValue,
          viewValue,
          temp,
          result,
          i;

        options = {
          types: ['(regions)'],
          componentRestrictions: { country: [] }
        };

        scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

        createViewValue = function (string) {
          result = '';
          temp = string.split(',');

          for (i = 0; i < temp.length; i++) {
            if (i > 0) {
              result += '_';
            }

            result += temp[i].trim().replace(/([a-z])([A-Z])/g, '$1_$2');
            result = result.split(' _').join(' ');
          }

          result = result.split('_').join(', ');

          return result;
        };

        google.maps.event.addListener(scope.gPlace, 'place_changed', function () {
          pacItems = document.getElementsByClassName('pac-item');
          console.log('hello');

          if (pacItems.length > 0) {
            viewValue = createViewValue(pacItems[0].innerText);

            scope.$apply(function () {
              element.val(viewValue);
            });
          }

          scope.$apply(function () {
            model.$setViewValue(element.val());
          });

          scope.getWeather(scope.model.place);
        });
      }
    };
  });
}());
