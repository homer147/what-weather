/* jslint node: true */
/* global inject, expect, jasmine, spyOn */

(function () {
  'use strict';

  describe('googlePlace directive', function () {
    var compile,
      element,
      scope,
      html,
      compiledElement,
      directiveElem;


    function getCompiledElement() {
      element = angular.element(html);
      compiledElement = compile(element)(scope);
      scope.$digest();
      return compiledElement;
    }

    beforeEach(function () {
      module('whatWeather');
    });

    beforeEach(inject(function ($compile, $rootScope) {
      compile = $compile;
      scope = $rootScope.$new();
      html = '<input type="text" name="place" ng-model="model.place" googleplace>';
    }));

    beforeEach(function () {
      directiveElem = getCompiledElement();
    });

    it('should successfully create directive object', function() {
      expect(directiveElem).toBeDefined();
    });

    it('should fire off an event when input is changed', function () {
      /*scope.getWeather = function () {
        return;
      };
      spyOn(scope, 'getWeather').and.callThrough();

      scope.gPlace = 'gladesvi';
      scope.$apply();

      expect(directiveElem).toHaveValue('gladesvi');
      expect(scope.gPlace).toEqual('gladesvi');
      expect(scope.getWeather).toHaveBeenCalled();*/
    });
  });
}());
