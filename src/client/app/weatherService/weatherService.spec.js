/* jslint node: true */
/* global inject, expect */

(function () {
  'use strict';

  describe('weatherService', function () {
    var weatherService,
      $httpBackend;

    beforeEach(function () {
      module('whatWeather');
    });

    beforeEach(inject(function (_weatherService_, $injector) {
      weatherService = _weatherService_;
      $httpBackend = $injector.get('$httpBackend');
    }));

    it('should ensure weatherService exists', function () {
      expect(weatherService).toBeDefined();
    });

    /*it('should get the weather for the provided place', function () {
      $httpBackend.when('GET', 'https://query.yahooapis.com/v1/public/yql').respond(200, [{response: 'hello!'}]);


      success = function (res) {
        console.log(res);
        //expect($httpBackend.flush).not.toThrow();
      };

      error = function (res) {
        console.log(res);
        //expect($httpBackend.flush).not.toThrow();
      };

      weatherService.getWeather('Santiago, Chile', success, error);

      expect($httpBackend.flush).not.toThrow();
    });*/

  });
}());
