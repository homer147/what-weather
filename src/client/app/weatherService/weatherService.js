(function () {
  'use strict';

  var app = angular.module('whatWeather');

  app.service('weatherService', ['$http', function ($http) {

    var getWeather,
      url,
      locationArray;

    getWeather = function (place, success, error) {
      locationArray = place.split(', ');

      if (locationArray.length < 2) {
        error({ error: 'search not specific enough' });
        return;
      }

      url = 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22' + encodeURI(locationArray[0]) + '%2C%20' + encodeURI(locationArray[1]) + '%22)%20and%20u%3D%27c%27&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys'; // jscs:ignore maximumLineLength

      $http.get(url)
      .then(function (response) {
        success(response);
      },

      function (response) {
        error(response);
      });
    };

    return {
      getWeather: getWeather
    };

  }]);
}());
