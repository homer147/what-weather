/*jslint node: true */
'use strict';

var gulp = require('gulp');
var config = require('./gulp.config')();
var $ = require('gulp-load-plugins')({lazy: true});
var Server = require('karma').Server;
var del = require('del');

gulp.task('help', $.taskListing);
gulp.task('default', ['help']);

gulp.task('build', ['lint', 'jscs', 'styles']);

gulp.task('lint', function () {
  log('Linting the js code');
  return gulp
    .src(config.js)
    .pipe($.jshint())
    .pipe($.jshint.reporter('jshint-stylish', {verbose: true, codeCol: 'green'}));
});

gulp.task('jscs', function () {
  log('Checking code against style guide');
  return gulp
    .src(config.js)
    .pipe($.jscs())
    .pipe($.jscs.reporter());
});

gulp.task('styles', ['clean-styles'], function() {
  log('Compiling Less --> CSS');
  return gulp
    .src(config.less)
    .pipe($.plumber())
    .pipe($.less())
    .pipe($.autoprefixer({browsers: ['last 2 versions', '> 5%']}))
    .pipe(gulp.dest(config.css));
});

gulp.task('clean-styles', function() {
  return clean(config.css + 'styles.css');
});

gulp.task('test', function(done) {
  log('Running Karma');
  return new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true
  }, done).start();
});

function clean(path) {
  log('Cleaning: ' + $.util.colors.green(path));
  return del(path);
}

function log(msg) {
  if (typeof(msg) === 'object') {
    for (var item in msg) {
      if(msg.hasOwnProperty(item)) {
        $.util.log($.util.colors.green(msg[item]));
      }
    }
  } else {
    $.util.log($.util.colors.green(msg));
  }
}
